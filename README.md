# Cross-platform Desktop Application Development: Electron, Node, NW.js, and React

Contains code and notes from studying [Cross-platform Desktop Application Development: Electron, Node, NW.js, and React](https://www.packtpub.com/web-development/cross-platform-desktop-application-development-electron-node-nwjs-and-react

## Projects

### Project: File explorer

Directory `nwjs-file-explorer` contains file explorer written with NW.js

#### Application _blueprint_

This was the list of user stories to it:

```
As a user, I can see the content of the current directory
As a user, I can navigate through the filesystem
As a user, I can open a file in the default associated program
As a user, I can delete a file
As a user, I can copy a file in the clipboard and paste it later in a new location
As a user, I can open the folder containing the file with the system file manager
As a user, I can close the application window
As a user, I can minimize the application window
As a user, I can maximize and restore the application window
As a user, I can change the application language
```

#### HTML prototype

I found this to be very clear explanation on [BEM](https://en.bem.info/methodology/key-concepts/). Excerpts below.

Having the following html:

```html
<article class="post">
  <h2 class="post__title">
    Star Wars: The Last Jedi's red font is a cause for concern
  </h2>
  <time datetime="2017-01-23 06:00" class="post__time">Jan 23, 2017</time>
</article>
```

The block we define with class `post`. It has two elements: `post__title` and `post__time`.

If we want to highlight one element on the post we could add a `post--sponsored` modifier to the block's classes:

```html
<article class="post post--sponsored">....</article>
```

In addition the author suggests using [Pragmatic CSS styleguide](https://github.com/dsheiko/pcss). He borrows ideas from it:

- Name with prefixes `is-` and `has-` classes that represent global states. Example: `is-hidden`, `has-error`
- Prefix layout related classes with `l-`, for example `l-app`
- Split the CSS files into folders: `Component` and `Base`.

This is the initial structure of the html `index.html`:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>File Explorer</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="./assets/css/app.css" rel="stylesheet" type="text/css" />
  </head>
  <body class="l-app">
    <header class="l-app__titlebar titlebar"></header>
    <div class="l-app__main l-main">
      <aside class="l-main__dir-list dir-list"></aside>
      <main class="l-main__file-list file-list"></main>
    </div>
    <footer class="l-app_footer footer"></footer>
  </body>
</html>
```

The CSS is split into files:

```bash
D:.
│   app.css
│
├───Base
│       base.css
│       defenitions.css
│
└───Component
        dir-list.css
        file-list.css
        footer.css
        icon.css
        l-app.css
        l-main.css
        titlebar.css
```

Then we have `./assets/css/Base/base.css` file with the following classes:

```css
html {
  -webkit-font-smoothing: antialiased;
}

* {
  box-sizing: border-box;
}

nav > ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

body {
  min-height: 100vh;
  margin: 0;
  font-family: Arial;
}

.is-hidden {
  display: none !important;
}
```

Or the application layout `./assets/css/Component/l-app.css` is:

```css
.l-app {
  display: flex;
  flex-flow: column nowrap;
  align-items: stretch;
}

.l-app__titlebar {
  flex: 0 0 40px;
}

.l-app__main {
  flex: 1 1 auto;
}

.l-app__footer {
  flex: 0 0 40px;
}
```

#### Fulfilling the functional requirements

It begins with introduction to ES2015/ES2016.

The JavaScript code is split into View and Service classes.

For handling window action the author uses `data-bind` attributes such as:

```html
<a class="titlebar__btn" data-bind="close">[X]</a>
```

For this we have the `TitleBarActionsView`:

```js
class TitleBarActionsView {
  constructor(boundingEl) {
    this.closeEl = boundingEl.querySelector('[data-bind=close]')
    this.bindUi()
  }

  bindUi() {
    this.closeEl.addEventListener('click', this.onClose.bind(this), false)
  }

  onClose(e) {
    e.preventDefault()
    nw.Window.get().close()
  }
}

exports.TitleBarActionsView = TitleBarActionsView
```

And the usage in `./js/app.js` is. We import the `TileBarActionView` class from the `./js/View/TitleBarActions` module and make an instance of it. We pass the first document element matching selector `[data-bind=titlebar]` to the class constructor:

```js
const { TitleBarActionsView } = require('./js/View/TitleBarActions')
new TitleBarActionsView(document.querySelector('[data-bind=titlebar]'))
```

The author gives great example of using [EventEmitter](https://nodejs.org/api/events.html) with the `DirService` class from `./js/Service/Dir.js`.

```js
//....
const EventEmitter = require('events')

class DirService extends EventEmitter {
  constructor(dir = null) {
    super()
    this.dir = dir || process.cwd()
  }
  setDir(dir = '') {
    let newDir = path.join(this.dir, dir)
    // Early exit
    if (DirService.getStats(newDir) === false) {
      return
    }
    this.dir = newDir
    this.notify()
  }

  notify() {
    this.emit('update')
  }
  //...
}
```

Extending `DirService` with the `EventEmitter`, gives us the possibility to fire service events and to subscribe on them:

```js
dirService.on('customEvent', () => console.log('fired customEvent'))
dirService.emit('customEvent')
```

#### Internationalization

Added `LangSelectorView` to bind the language selector from the UI to the `set locale()` of `I18nService`.

Localization of date/time format and using dictionary with localized strings.

Added internationalization service `I18nService`.  
Maintains the current locale name.  
Is event emitter and views subscribe to changes of the locales. See `FileListView`.  
Injected with dictionary via constructor (dictionary exported from data file `js/Data/dictionary.js`).  
Exposes `translate()`, the views use it to present localized strings.  
Implements getter/setter:

```js
  get locale() {
    return this._locale
  }
  set locale(locale) {
    this._locale = locale
  }
```

The start JS file `app.js` does the dependency injection.

## Links

Useful links:

- [WireframeSketcher](https://wireframesketcher.com/)
- [NW.js](https://nwjs.io/)
- [NW.js docu](http://docs.nwjs.io/en/latest/)
- [NW.js manifest format](https://github.com/nwjs/nw.js/wiki/manifest-format)
- [package.json fields](https://docs.npmjs.com/files/package.json)
- [All you neeed to know about Flexbox](https://medium.freecodecamp.org/understanding-flexbox-everything-you-need-to-know-b4013d4dc9af)
- [CSS code smells](https://docs.npmjs.com/files/package.json)
- [BEM key concepts](https://en.bem.info/methodology/key-concepts/)
- [Pragmatic CSS styleguide](https://github.com/dsheiko/pcss)
- [CSS variables](https://www.w3.org/TR/css-variables)
- [CSS sticky position](https://www.w3.org/TR/css-position-3/#sticky-pos)
- [EventEmitter](https://nodejs.org/api/events.html)
