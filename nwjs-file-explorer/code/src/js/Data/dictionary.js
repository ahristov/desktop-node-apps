exports.dictionary = {
  "en-US": {
    NAME: "Name",
    SIZE: "Size",
    MODIFIED: "Modified",
    MINIMIZE_WIN: "Minimize window",
    RESTORE_WIN: "Restore window",
    MAXIMIZE_WIN: "Maximize window",
    CLOSE_WIN: "Close window"
  },
  "de-DE": {
    NAME: "Dateiname",
    SIZE: "Größe",
    MODIFIED: "Geändert am",
    MINIMIZE_WIN: "Fenster minimieren",
    RESTORE_WIN: "Fenster wiederherstellen",
    MAXIMIZE_WIN: "Fenster maximieren",
    CLOSE_WIN: "Fenster schließen"
  },
  "bg-BG": {
    NAME: "Име",
    SIZE: "Размер",
    MODIFIED: "Последно промемен",
    MINIMIZE_WIN: "Смали",
    RESTORE_WIN: "Нормален размер",
    MAXIMIZE_WIN: "Цял екран",
    CLOSE_WIN: "Затвори"
  }
};